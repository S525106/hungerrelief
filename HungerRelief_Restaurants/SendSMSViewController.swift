//
//  SendSMSViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Priyanka on 4/16/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//


import UIKit
import MessageUI



class SendSMSViewController: UIViewController,MFMessageComposeViewControllerDelegate{

    var phoneNumber:String!
    @IBOutlet weak var phoneNumberTF: UILabel!
          override func viewDidLoad() {
            super.viewDidLoad()
            phoneNumberTF.text = phoneNumber
            // Do any additional setup after loading the view.
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
    
        @IBAction func sendSMS(sender: AnyObject) {
            
            let messageVC = MFMessageComposeViewController()
            
            messageVC.body = "Items are ready to pick";
            messageVC.recipients = ["8302129569"]
            messageVC.messageComposeDelegate = self;
            
            self.presentViewController(messageVC, animated: false, completion: nil)
            
            
        }
        func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
            switch (result.rawValue) {
            case MessageComposeResultCancelled.rawValue:
                print("Message was cancelled")
                self.dismissViewControllerAnimated(true, completion: nil)
            case MessageComposeResultFailed.rawValue:
                print("Message failed")
                self.dismissViewControllerAnimated(true, completion: nil)
            case MessageComposeResultSent.rawValue:
                print("Message was sent")
                self.dismissViewControllerAnimated(true, completion: nil)
            default:
                break;
            }
        }
        
    }


