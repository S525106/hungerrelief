//
//  ListTableViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/17/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit

class ListTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,Operation {
    
    var kinveyClientOperation:KinveyOperation!
    var array:[Item] = []
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        print("list controller view did load")
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem=nil
        kinveyClientOperation = KinveyOperation(operation:self)
        kinveyClientOperation.FetchItem()
        
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        // return listArray.count
        return 1
    }
    //    override func viewWillAppear(animated: Bool) {
    //        kinveyClientOperation.FetchItem()
    //    }
    
    func fetch(item: Item) {
        array.append(item)
        print(array.description)
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
//        
//        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
//        
//        let typeLbl : UILabel = cell.viewWithTag(12) as! UILabel // function viewWithTag(Int) to update the labels in the table.
//        
//        let quantityLbl:UILabel = cell.viewWithTag(78) as! UILabel
//        
//        nameLbl.text = array[indexPath.row].itemName
//        
//        typeLbl.text = array[indexPath.row].type
//        
//        quantityLbl.text = array[indexPath.row].quantity
//        
        
        return cell
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
