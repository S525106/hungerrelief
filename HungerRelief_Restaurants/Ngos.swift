//
//  Ngos.swift
//  HungerRelief_Restaurants
//
//  Created by Valleshetti,Anwesh on 4/14/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation


class Ngos:NSObject{
    var ngosName:String
    var emailId:String
    var password:String
    var address:String
    var city:String
    var state:String
    var country:String
    var phoneNumber:String
    var latitude:CLLocationDegrees
    var longitude:CLLocationDegrees
    var entityId: String?
    override init(){
        self.ngosName = ""
        self.emailId = ""
        self.password = ""
        self.address = ""
        self.city = ""
        self.state = ""
        self.country = ""
        self.phoneNumber = ""
        self.latitude = 0.0
        self.longitude = 0.0
    }
    
    init(ngosName:String,emailId:String,password:String,address:String,city:String,state:String,country:String,phoneNumber:String,latitude:CLLocationDegrees,longitude:CLLocationDegrees){
        
        self.ngosName = ngosName
        self.emailId = emailId
        self.password = password
        self.address = address
        self.city = city
        self.state = state
        self.country = country
        self.phoneNumber = phoneNumber
        self.latitude = latitude
        self.longitude = longitude
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "ngosName" : "ngosName",
            "emailId" : "emailId",
            "password" : "password",
            "address":"address",
            "city":"city",
            "state":"state",
            "country":"country",
            "phoneNumber":"phoneNumber",
            "latitude":"latitude",
            "longitude":"longitude"
            
            
        ]
    }
    
}
