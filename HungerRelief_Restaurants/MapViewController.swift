//
//  MapViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Gaddam,Vishnu Tulasi on 3/15/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation
import MessageUI
class MapViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate ,Operation{
    
    
    
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var myLocations: [CLLocation] = []
     var matchingItems: [MKMapItem] = [MKMapItem]()
    var res:Restaurant!
    var kinveyClientOperation:KinveyOperation!
    var ngosLocations:[Ngos] = []
    var closestLocation:CLLocation!
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
     func getLocations(sender: AnyObject) {
        
        
        locationInLocations(ngosLocations, closestToLocation: closestLocation)
        
    }
    
    
    func locationInLocations(locations: [Ngos], closestToLocation location: CLLocation) -> CLLocation? {
        
        
    
        
        let smallestDistance: CLLocationDistance = 200000
        
        
        
        
        var closLocs:[Ngos] = []
        for loc in locations {
            let distance = location.distanceFromLocation(CLLocation(latitude: loc.latitude, longitude: loc.longitude))
            if  distance == 0 || distance < smallestDistance {
                closestLocation = CLLocation(latitude: loc.latitude, longitude: loc.longitude)
                //smallestDistance = distance
                closLocs.append(loc)
               
                
            }
        }
       let  mapView1:MKMapView
        for cl in closLocs {
            
            var here1:MKPointAnnotation = MKPointAnnotation()
        here1.title = cl.ngosName
        here1.subtitle = cl.phoneNumber
            
        
        let lat = cl.latitude
       
        let long = cl.longitude
            
       
        here1.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        mapView.addAnnotation(here1)
        
        mapView.setCenterCoordinate(here1.coordinate, animated: true)
            let span = MKCoordinateSpanMake(0.005, 0.005)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)

        //myMapView.reloadInputViews()
       
        }
        return closestLocation
    }
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
       
    }
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!)
        -> MKAnnotationView! {
        var pinView:MKPinAnnotationView! = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") as!
        MKPinAnnotationView!
        if pinView == nil {
        pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        pinView.pinTintColor = UIColor.greenColor()
        pinView.canShowCallout = true
        let callBTN = UIButton(type: UIButtonType.DetailDisclosure)
        
        pinView.rightCalloutAccessoryView = callBTN
        callBTN.addTarget(self, action: Selector("showMoreInfo:"),
        forControlEvents: UIControlEvents.TouchUpInside)
        }
        return pinView
    }

    func showMoreInfo(sender:UIButton!){
            
}
    func mapView(mapView: MKMapView!, annotationView: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            
            //get tag here
            if(annotationView.tag == 0){
            //Do for 0 pin
            }
              //let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
            if control == annotationView.rightCalloutAccessoryView {
                let message =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("message") as! SendSMSViewController
               
                message.phoneNumber = (annotationView.annotation?.subtitle)!
                self.navigationController?.pushViewController(message, animated: true)
            }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
         kinveyClientOperation = KinveyOperation(operation:self)
        kinveyClientOperation.fetchCoordinates(userValue)
        kinveyClientOperation.fetchNgo()
          self.navigationItem.title = "Get Locations"
         let logout: UIBarButtonItem = UIBarButtonItem(title: "Locations", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("getLocations:"))
      
        self.navigationItem.rightBarButtonItem = logout
        
//        self.locationManager.delegate = self
//        
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        
//        self.locationManager.requestWhenInUseAuthorization()
//        
//        self.locationManager.startUpdatingLocation()
//        self.mapView.showsUserLocation = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    func fetchcord(restaurant:Restaurant){
        
        myLocations.append(CLLocation(latitude: restaurant.lat, longitude: restaurant.long))
        closestLocation = myLocations[0]
        let latitude:CLLocationDegrees = restaurant.lat
        let longitude:CLLocationDegrees = restaurant.long
        let location = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
        // 2
        let span = MKCoordinateSpanMake(0.0005, 0.0005)
        let region = MKCoordinateRegion(center: location, span: span)
    mapView.setRegion(region, animated: true)
        
        

        self.mapView.delegate = self

      print(myLocations.count)
        
    }
    func fetchNgoRecord(ngo:[Ngos]){
   ngosLocations = ngo
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let location = locations.last
//        
//        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//        
//        self.mapView.setRegion(region, animated: true)
//    }
//    //    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//    //        let location = locations.last
//    //
//    //        let center = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
//    //
//    //        let region = MKCoordinateRegionMake(center, MKCoordinateSpanMake(1, 1))
//    //
//    //
//    //        self.mapView.setRegion(region, animated: true)
//    //
//    //        self.locationManager.stopUpdatingLocation()
//    //    }
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        
//        print("Errors"+error.localizedDescription)
//}
//
//    @IBAction func textFieldReturn(sender: AnyObject) {
//        sender.resignFirstResponder()
//        mapView.removeAnnotations(mapView.annotations)
//        self.performSearch()
//    }
//    func performSearch() {
//        
//        matchingItems.removeAll()
//        let request = MKLocalSearchRequest()
//        request.naturalLanguageQuery = searchText.text
//        request.region = mapView.region
//        
//        let search = MKLocalSearch(request: request)
//        
//        func startWithCompletionHandle(response:MKLocalSearchResponse!,error: NSError!) {
//            
//            if error != nil {
//                print("Error occured in search: \(error.localizedDescription)")
//            } else if response.mapItems.count == 0 {
//                print("No matches found")
//            } else {
//                print("Matches found")
//                
//                for item in response.mapItems {
//                    print("Name = \(item.name)")
//                    print("Phone = \(item.phoneNumber)")
//                    
//                    self.matchingItems.append(item as MKMapItem)
//                    print("Matching items = \(self.matchingItems.count)")
//                    
//                    let annotation = MKPointAnnotation()
//                    annotation.coordinate = item.placemark.coordinate
//                    annotation.title = item.name
//                    self.mapView.addAnnotation(annotation)
//                }
//            }
//        }
//    }
}

