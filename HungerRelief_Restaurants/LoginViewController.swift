//
//  LoginViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,Operation{
    
    @IBOutlet weak var restaurantNameTF: UITextField!
    
    @IBOutlet weak var EmailIdTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    var kinveyClientOperation:KinveyOperation!
    var  restaurantlogin:RestaurantLogin!

   override func viewDidLoad() {
       super.viewDidLoad()
         kinveyClientOperation = KinveyOperation(operation:self)
//          self.navigationController?.navigationBar.backItem?.hidesBackButton = true
        self.navigationItem.backBarButtonItem=nil

        // Do any additional setup after loading the view.
    }

    @IBAction func Login(sender: AnyObject) {
        if (EmailIdTF.text!.isEmpty || passwordTF.text!.isEmpty){
        self.displayAlertControllerWithTitle("Login Failed", message: "All fields are Required to fill")
        }
        else{
        restaurantlogin = RestaurantLogin(emailId: EmailIdTF.text!, password: passwordTF.text!)
        kinveyClientOperation.login(restaurantlogin)
             
        }
    }
    func loginFailed() {
        self.displayAlertControllerWithTitle("Login failed", message:"TryAgain")
    }
    func onSuccess(){
        self.displayAlertControllerWithSuccess("Login Successfull", message:"")
    }
    override func viewWillAppear(animated: Bool) {
      self.navigationItem.setHidesBackButton(true, animated: false)
    }

   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    func displayAlertControllerWithSuccess(title:String, message:String) {
        print("login success")
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("loginSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
