//
//  button.swift
//  HungerRelief_Restaurants
//
//  Created by srikanth chippa on 16/04/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit
class button:UIButton{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.borderColor = tintColor.CGColor
        layer.cornerRadius = 5.0
        clipsToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        setTitleColor(UIColor.blackColor(), forState: .Normal)
        setBackgroundImage(UIImage(named:"yellow.jpg")!, forState: .Normal)
    }
}