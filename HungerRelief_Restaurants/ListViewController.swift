//
//  ListViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 4/1/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UIApplicationDelegate,UITableViewDataSource,Operation {
    
    var kinveyClientOperation:KinveyOperation!
    var array:[Item] = []
    @IBOutlet weak var tableView: UITableView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
               super.viewDidLoad()
        self.navigationItem.title = "VIEW ITEMS"
        
        self.navigationItem.backBarButtonItem=nil
        kinveyClientOperation = KinveyOperation(operation:self)
       
        let logout: UIBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("logout1:"))
        
        
        
        let editProfile: UIBarButtonItem = UIBarButtonItem(title: "map", style: UIBarButtonItemStyle.Plain, target: self, action:Selector("map:"))
        
        self.navigationItem.setRightBarButtonItems([logout,editProfile], animated: true)
        
        
        
       
        
        let sortBY: UIBarButtonItem = UIBarButtonItem(title: "Add Item", style: UIBarButtonItemStyle.Plain, target: self, action: "Add")
        
        
        
        self.navigationItem.leftBarButtonItem = sortBY
        

        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
         kinveyClientOperation.FetchItem()
        self.tableView.reloadData()

       
    }
    func logout1(Any:AnyObject){
        
        if KCSUser.activeUser() != nil {
            
            KCSUser.activeUser().logout()
            
            //displayAlertControllerWithTitle("Success", message:"logged out!")
            
            let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
            
            self.navigationController?.pushViewController(login, animated: true)
            
        }
        
        
    }
    func map(Any:AnyObject){
        
        print(" ")
            
            let map3 =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("map") as! MapViewController
            
            self.navigationController?.pushViewController(map3, animated: true)
            
        }
    
    func Add(){
        
                  let add =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("Add") as! ItemsViewController
            
            self.navigationController?.pushViewController(add, animated: true)
            
        }
        

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
         return array.count
           }
    //    override func viewWillAppear(animated: Bool) {
    //        kinveyClientOperation.FetchItem()
    //    }
    
    func fetch(item: [Item]) {
        array = item
       
        
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
                let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        
                let typeLbl : UILabel = cell.viewWithTag(12) as! UILabel // function viewWithTag(Int) to update the labels in the table.
        
                let quantityLbl:UILabel = cell.viewWithTag(78) as! UILabel
        
                nameLbl.text = array[indexPath.row].itemName
        
                typeLbl.text = array[indexPath.row].type
        
                quantityLbl.text = array[indexPath.row].quantity
        
                
        
        return cell
    }
    


        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
