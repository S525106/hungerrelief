//
//  KinveyOperation.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation

@objc protocol Operation{
    //bridge for view conttroller and kinvey operation class
    
    optional func onSuccess()
    optional func onError(message:String)
    optional func noActiveUser()
    optional func loginFailed()
    optional func SignupSuccess()
    optional func Signupfailed()
    optional func fetch(item:[Item])
    optional func fetchcord(restaurant:Restaurant)
    optional func fetchNgoRecord(ngo:[Ngos])
    
}
class KinveyOperation{
    
    var restaurant:Restaurant!
    var restaurantlogin:RestaurantLogin!
    let store:KCSAppdataStore!
    let store1:KCSAppdataStore!
    let store2:KCSAppdataStore!
    let operationDelegate:Operation!
     var listArray:[Item] = []
      let defaults = NSUserDefaults.standardUserDefaults()
    
    
    
    
    
    init(operation:Operation){
        store = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Restaurant",
            KCSStoreKeyCollectionTemplateClass : Restaurant.self
            ])
        store1 = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Item",
            KCSStoreKeyCollectionTemplateClass : Item.self
            ])
        store2 = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Ngos",
            KCSStoreKeyCollectionTemplateClass : Ngos.self
            ])

        self.operationDelegate = operation
        }
    func saveData(){
        if let _ = KCSUser.activeUser(){
            
        }else{
            operationDelegate.noActiveUser!()
        }
    }
    
    func login(restaurantlogin:RestaurantLogin){
        KCSUser.loginWithUsername(
            restaurantlogin.emailId,
            password: restaurantlogin.password,
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operationDelegate.onSuccess!()
                    self.defaults.setValue(restaurantlogin.emailId, forKey: Constants.USERNAME)
                    
                    self.defaults.synchronize()
                    
                                    } else {
                   
                    self.operationDelegate.loginFailed!()
                                  }
            }
        )
    }
    func Register(let r:Restaurant){
     
        KCSUser.userWithUsername(
           r.emailId,
            password:r.password ,
            fieldsAndValues: [
      KCSUserAttributeGivenname : r.restaurantName,
                           ],
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operationDelegate.SignupSuccess!()
                    self.saveData()
                    
                    self.store.saveObject(
                        r,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                            } else {
                                //save was successful
                                print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            }
                        },
                        withProgressBlock: nil
                    )

                    
                } else {
                    self.operationDelegate.Signupfailed!()
                   
                }
            }
        )
    }
   
    func SaveItem(let item:Item){
        
        self.store1.saveObject(
            item,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //save failed
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                } else {
                    //save was successful
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                }
            },
            withProgressBlock: nil
        )

    }
    func fetchCoordinates(emailId:String){
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: emailId)
        store.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil {
                    if let objects = objectsOrNil as [AnyObject]!{
                        for object in objects{
                            let item = object as! Restaurant
                            self.operationDelegate.fetchcord!(item)
                            
                            //print(self.listArray.)
                        }
                    }
                            
                }
                else {
                    
                    print("Error")
                    }
            
            },
            withProgressBlock: nil
        )
        
    }
    func fetchNgo(){
        
        store2.queryWithQuery(
            KCSQuery(),
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil == nil {
                    
                    let objects = objectsOrNil as! [Ngos]
                    
                    self.operationDelegate.fetchNgoRecord!(objects)

                    
                }
                else {
                    
                    print("Error")
                }
                
            },
            withProgressBlock: nil
        )

    }
    
    func FetchItem() {
        //listArray = ""
         let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: userValue)
       
        store1.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
               
                if errorOrNil == nil {
                    
                    let objects = objectsOrNil as! [Item]
                    
                    self.operationDelegate.fetch!(objects)
                    
//                    if let objects = objectsOrNil as! [Item]{
////                        for object in objects{
////                             let item = object as! Item
//                             self.operationDelegate.fetch!([objects])
//                            
//
//                       
//                       //  print(item.itemName)
////                            
////                            
////                            self.listArray.append(item)
//                            
//                            //print(self.listArray.)
//                       // }
//                    }
                    // print("****** \(self.listArray)")
                    
                } else {
                    
                    print("Error")
                   
                }
      },
            withProgressBlock: nil
        )
       
        }
    
    
    
}

