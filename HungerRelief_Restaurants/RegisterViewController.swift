//
//  RegisterViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController ,Operation{
    
   
    @IBOutlet weak var RestaurantNameTF: UITextField!
   
    @IBOutlet weak var EmailIdTF: UITextField!
    
    @IBOutlet weak var PasswordTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    
    @IBOutlet weak var cityTF: UITextField!
    
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    @IBOutlet weak var zipTF: UITextField!
   
    @IBOutlet weak var stateTF: UITextField!
   
    @IBOutlet weak var countryTF: UITextField!
    
    @IBOutlet weak var phoneNumberTF: UITextField!
    var lat:CLLocationDegrees = 0.0
    var long:CLLocationDegrees = 0.0
    var kinveyClientOperation:KinveyOperation!
   

   
    var  restaurant:Restaurant!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        
         kinveyClientOperation = KinveyOperation(operation:self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func isValidEmail(testStr:String) -> Bool {
        
        //  println("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    @IBAction func Register(sender: AnyObject) {
        
        
        if (RestaurantNameTF.text!.isEmpty || EmailIdTF.text!.isEmpty || PasswordTF.text!.isEmpty){
            self.displayAlertControllerWithTitle("Registration Failed", message: "All fields are Required to fill")
        }
        else if  PasswordTF.text != confirmPasswordTF.text! {
            self.displayAlertControllerWithTitle("Mismatch", message: "password doesnot match")
        }
        else if (!isValidEmail(EmailIdTF.text!))
        {
            self.displayAlertControllerWithTitle("Invalid Email", message: "insert proper email address")
        }
            
        else{
            
            self.geo()
            
        }
    }
    func geo(){
        let geoCoder = CLGeocoder()
        let addressString = "\(addressTF.text) \(cityTF.text) \(stateTF.text) \(zipTF.text)"
        
        geoCoder.geocodeAddressString(addressString, completionHandler:
            {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                } else if placemarks!.count > 0 {
                    print("hey")
                    
                    let placemark = placemarks![0] as! CLPlacemark
                    let location = placemark.location
                    let coords = location!.coordinate
                    self.lat = (location?.coordinate.latitude)!
                    self.long = (location?.coordinate.longitude)!
                    print(self.lat)
                    print(self.long)
                    print("hiiii")
                    self.restaurant = Restaurant(restaurantName: self.RestaurantNameTF.text!, emailId: self.EmailIdTF.text!, password: self.PasswordTF.text!, address:self.addressTF.text!, city: self.cityTF.text!, state: self.stateTF.text!, country: self.countryTF.text!, phoneNumber: self.phoneNumberTF.text!,lat: self.lat,long: self.long,zipCode: self.zipTF.text!)
                    self.kinveyClientOperation.Register(self.restaurant)
                   
                    
                    
                    
                    //                    let loc:CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.lat, self.long)
                    //                    var locations:[CLLocationCoordinate2D] = []
                    //                    locations.append(loc)
                    //self.nearlocations.locations.append(loc)
                    //self.kinveyClientOperation.Savelocation(nearLocation)
                    
                }
        })
        
    }

    func SignupSuccess() {
        self.displayAlertControllerWithSuccess("Registration successful", message: "Welcome!")
    }
    func onError(message: String) {
        self.displayAlertControllerWithTitle("Registration failed", message: message)
    }
    func noActiveUser() {
        print("noActiveUser")
        
    }
    
    func displayAlertControllerWithSuccess(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("registerSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
