//
//  ItemsViewController.swift
//  HungerRelief_Restaurants
//
//  Created by Gaddam,Vishnu Tulasi on 3/15/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import UIKit

class ItemsViewController: UIViewController,Operation {
    
    
    @IBOutlet weak var itemNameTF: UITextField!
    let defaults = NSUserDefaults.standardUserDefaults()
   

    @IBOutlet weak var typeTF: UITextField!
    
    @IBOutlet weak var quantityTF: UITextField!
    
    var kinveyClientOperation:KinveyOperation!
    var r:RestaurantLogin!
    
    var item:Item!
    
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
       // Do any additional setup after loading the view.
    }

  
    @IBAction func AddItem(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        item = Item(itemName: itemNameTF.text!, type:typeTF.text! , quantity:quantityTF.text!,emailId:userValue )
        kinveyClientOperation.SaveItem(item)
     self.dismissViewControllerAnimated(true, completion: nil)
        
    }
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
