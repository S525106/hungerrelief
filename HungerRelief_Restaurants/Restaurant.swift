//
//  Restaurant.swift
//  HungerRelief_Restaurants
//
//  Created by Kommula,Priyanka on 3/13/16.
//  Copyright © 2016 Kommula,Priyanka. All rights reserved.
//

import Foundation

class Restaurant:NSObject{
    var restaurantName:String
    var emailId:String
    var password:String
    var address:String
    var city:String
    var state:String
    var country:String
    var phoneNumber:String
    var lat:CLLocationDegrees
    var long:CLLocationDegrees
    var zipCode:String
    var entityId: String?
    override init(){
        self.restaurantName = ""
        self.emailId = ""
        self.password = ""
        self.address = ""
        self.city = ""
        self.state = ""
        self.country = ""
        self.phoneNumber = ""
        self.lat = 0.0
        self.long = 0.0
        self.zipCode = ""
    }
    
    init(restaurantName:String,emailId:String,password:String,address:String,city:String,state:String,country:String,phoneNumber:String,lat:CLLocationDegrees,long:CLLocationDegrees,zipCode:String){
        
        self.restaurantName = restaurantName
        self.emailId = emailId
        self.password = password
        self.address = address
       self.city = city
       self.state = state
        self.country = country
        self.phoneNumber = phoneNumber
        self.lat = lat
        self.long = long
        self.zipCode = zipCode
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
       "entityId" : KCSEntityKeyId, //the required _id field
            "restaurantName" : "restaurantName",
            "emailId" : "emailId",
            "password" : "password",
            "address":"address",
            "city":"city",
            "state":"state",
            "country":"country",
            "phoneNumber":"phoneNumber",
            "lat":"lat",
            "long":"long",
            "zipCode" : "zipCode"
        
        ]
    }

}